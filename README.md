# Pickup Dropoff with Time Windows

This repository contains the scripts for the **Pickup Drop-off with Time Windows** problem. It differs from the **Capacitated Vehicle Problem with Time Windows**
in that it allows us to have students from multiple schools on a single bus.

# Virtual Environment
The virtual enviroment which contains the packages required for the main script is found in .venv. Select it as the default interpreter in VSCode.