"""Simple Pickup Delivery Problem (PDP) with Time Windows"""

from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
import numpy as np
import pandas as pd
import datetime          as dt
import csv
import sys
import configparser
import requests
import json
import getopt
import time
import datetime
import folium
import math


locationAray = [(1,2),
                (3,4),
                (5,6),
                (7,8)]

stopNum = ["D","P1","Del","P2"]
school_code = str("Temp")

def create_data_model():
    """Stores the data for the problem."""
    data = {}
    data['distance_matrix'] = [
        [
            0, 0, 0, 0
        ],
        [
            548, 0, 684, 308
        ],
        [
            776, 684, 0, 992
        ],
        [
            696, 308, 992, 0
        ],
    ]
    data['pickups_deliveries'] = [
        [1, 2],
        [3, 2],
    ]
    data['time_matrix'] = [
        [
            0, 0, 0, 0
        ],
        [
            274, 0, 342, 154
        ],
        [
            388, 342, 0, 496
        ],
        [
            348, 154, 496, 0
        ],
    ]
    
    data['time_windows'] = [[0,5000],[0,5000],[0,5000],[0,5000]]
    
    data['demands']      = [0,2,-5,3]
    
    data['vehicle_capacities'] = [10,10]
    data['num_vehicles'] = 2
    data['depot'] = 0
    data['LocationArray']=[(1,2),
                (3,4),
                (5,6),
                (7,8)]

    return data


def print_solution(data, manager, routing, assignment):
    """Prints assignment on console."""
    total_distance = 0
    time_dimension = routing.GetDimensionOrDie('Time')
    total_load     = 0
    total_time     = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        route_load     = 0
        while not routing.IsEnd(index):
            time_var = time_dimension.CumulVar(index)
            plan_output += '{0} Time({1},{2}) -> '.format(
                manager.IndexToNode(index), assignment.Min(time_var),
                assignment.Max(time_var))
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            plan_output += ' {0} Load( {1})-> '.format(node_index, route_load)
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            route_distance += (routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id))
        time_var = time_dimension.CumulVar(index)
        plan_output += '{0} Time({1},{2})\n'.format(
            manager.IndexToNode(index), assignment.Min(time_var),
            assignment.Max(time_var))
        plan_output += 'Time of the route: {}secs\n'.format(
            assignment.Min(time_var))
        plan_output += '{0} Load( {1})\n'.format(
                manager.IndexToNode(index), route_load)
        plan_output += 'Distance of the route: {}m\n'.format(route_distance)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
        total_time += assignment.Min(time_var)
    print('Total Distance of all routes: {}m'.format(total_distance))
    print('Total load of all routes:{}'.format(total_load))
    print('Total time of all routes: {} secs'.format(total_time))


def print_save_solution2(data, manager, routing, assignment, locationArray,distance_matrix,school_code):
    """Prints solution on console."""
    vehicle_id                  = []
    sequence_within_vehicle     = []
    # earliest_scheduled_time     = [-1] * (data.num_locations)
    # latest_scheduled_time       = [-1] * (data.num_locations)
    #vehicle_cumulative_time     = [0]  * (data.num_locations)
    vehicle_cumulative_distance = []  
    vehicle_cumulative_time     = []
    #pick_drop = ["depot"] + ["pick","drop"] * (data.num_locations // 2)
    loadsfordf                  = []
    route_time                  = [0] * (data['num_vehicles'])
    locationsfordf              = []
    indextoNodetracker          = []
    
    #passenger_time_in_shuttle = [0] * (data['num_locations'])
    #total_time = 0
    time_dimension = routing.GetDimensionOrDie('Time')
    for veh_id in range(data['num_vehicles']):
       index = routing.Start(veh_id)
       route_distance = 0
       route_load = 0
       #route_distance = 0
       sequence_counter = 0 # Counts the sequence number of a location within the vehicle's route that it is assigned to
       while not routing.IsEnd(index):
          time_var = time_dimension.CumulVar(index)
          node_index = manager.IndexToNode(index)
          indextoNodetracker.append(node_index)
          route_load += data['demands'][node_index]
          locationsfordf.append(locationArray[node_index])
          vehicle_id.append(veh_id)
          sequence_within_vehicle.append(sequence_counter)
          vehicle_cumulative_time.append((dt.datetime(1900,1,1,6,10) + dt.timedelta(seconds=float(assignment.Value(time_var)))).time())
          vehicle_cumulative_distance.append(route_distance)
          loadsfordf.append(route_load)
          
          previous_index = index
          index = assignment.Value(routing.NextVar(index))
          next_node_index = manager.IndexToNode(index)
          sequence_counter = sequence_counter + 1
          route_distance  += distance_matrix[node_index][next_node_index]
       time_var = time_dimension.CumulVar(index)
       route_time[veh_id] += assignment.Min(time_var)
    data_for_df = [(
                vehicle_id[i],sequence_within_vehicle[i],locationsfordf[i][1],locationsfordf[i][0],
                vehicle_cumulative_distance[i], loadsfordf[i], vehicle_cumulative_time[i], indextoNodetracker[i]) for i in range(len(loadsfordf))]
    #            earliest_scheduled_time[i],latest_scheduled_time[i]
    
        
        
    data_df = pd.DataFrame(data=data_for_df,columns=[
    #                                            "location_NS","location_EW",
                                                 "Vehicle ID","sequence #","Lat","Long",
                                                 "vehicle cumul dist","cumul demands","arrival time",
                                                 "indextonode tracker"])
    #                                            "earliest scheduled time","latest scheduled time"])
    data_df = data_df.drop(data_df[data_df['sequence #'] == 0].index)
    #unique_vehicles = list(set(data_df['Vehicle ID']))
    
    #for i in range(len(unique_vehicles)):
        #vehicle_id = unique_vehicles[i]
        ## Find last ocurrence in df of this vehicle id
        #last_index = data_df[data_df['Vehicle ID'] == i].last_valid_index()
        #print (last_index)
        ## Get time of arrival at last pickup spot
        #time_of_arrival = dt.datetime.strptime(data_df['arrival time'][last_index], "%H:%M:%S")
        #index_node = data_df['indextonode tracker'][last_index]
        #time_to_school = time_of_arrival + dt.timedelta(minutes = (times[index_node])[5])
        #print (time_to_school)
        
    #path_to_save_to = "D:/PDPTW/"
    path_to_save_to_2 = "str(school_code)"+".csv"
    path_to_save_to_3 = "str(school_code)"+"-veh.csv"
    data_df.to_csv(path_to_save_to_2, index=False)
    vehicletimes = [(
            veh_id, route_time[veh_id]) for veh_id in range(data['num_vehicles'])]
    vehicletimesdf = pd.DataFrame(data=vehicletimes, columns=[
            "vehicle ID", "totalmins"])
    vehicletimesdf['totalmins'] = vehicletimesdf['totalmins'].apply(lambda x: x/60)
    
    vehicletimesdf.to_csv(path_to_save_to_3, index=False)


def main():
    """Entry point of the program."""
    # Instantiate the data problem.
    data = create_data_model()

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)

    
    def time_callback(from_index, to_index):
        """Returns the travel time between the two nodes."""
        # Convert from routing variable Index to time matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['time_matrix'][from_node][to_node]

    transit_time_callback_index = routing.RegisterTransitCallback(time_callback)
    #routing.SetArcCostEvaluatorOfAllVehicles(transit_time_callback_index)
    

    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]
    
    demand_callback_index = routing.RegisterUnaryTransitCallback(demand_callback)
    routing.AddDimensionWithVehicleCapacity(demand_callback_index,
                                            0, #nullcapacity slack
                                            data['vehicle_capacities'], #vehicle max capacities
                                            True, #start cumul to 0
                                            'Capacity')    


    # Define cost of each arc.
    def distance_callback(from_index, to_index):
        """Returns the manhattan distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Add Distance constraint.
    dimension_name = 'Distance'
    routing.AddDimension(
        transit_callback_index,
        0,  # no slack
        3000,  # vehicle maximum travel distance
        True,  # start cumul to zero
        dimension_name)
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    distance_dimension.SetGlobalSpanCostCoefficient(100)
    
    # Add Time constraint.
    
    time = 'Time'
    routing.AddDimension(
        transit_time_callback_index,
        5000,  # allow waiting time per vehicle
        5000,  # maximum time per vehicle
        False,  # Don't force start cumul to zero.
        time)
    time_dimension = routing.GetDimensionOrDie(time)

    for location_idx, time_window in enumerate(data['time_windows']):
        if location_idx == 0:
            continue
        index = manager.NodeToIndex(location_idx)
        time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])
    # Add time window constraints for each vehicle start node.
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        time_dimension.CumulVar(index).SetRange(data['time_windows'][0][0],
                                                data['time_windows'][0][1])



    # Define Transportation Requests.
    for request in data['pickups_deliveries']:
        pickup_index = manager.NodeToIndex(request[0])
        delivery_index = manager.NodeToIndex(request[1])
        routing.AddPickupAndDelivery(pickup_index, delivery_index)
        routing.solver().Add(
            routing.VehicleVar(pickup_index) == routing.VehicleVar(
                delivery_index))
        routing.solver().Add(
            distance_dimension.CumulVar(pickup_index) <=
            distance_dimension.CumulVar(delivery_index))

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_MOST_CONSTRAINED_ARC)

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if solution:
        print_solution(data, manager, routing, solution)
        print_save_solution2(data, manager, routing, solution, data['LocationArray'],data['distance_matrix'],school_code)

if __name__ == '__main__':
    main()

    