"""Dividing each 30 minutes time slot into further smaller slots"""

import pandas as pd
import numpy as np
import datetime as dt
import csv
import sys
import configparser
import requests
import json
import getopt
import time
import googlemaps


## The logic is that we need a smaller amount of students for each routing attempt to
## 1) Obtain smaller distance matrices and 2) Get results (sometimes, OR Tools doesn't work with large datasets)

############################################################################
############################################################################

Google_APIKEY = "AIzaSyAcaEeaU-H9G-enF7ZUYsoFSdNpAu2t7po"

school_df = pd.read_excel("MorningsSLOT1.xlsx")

students_df = pd.read_excel("RI Mornings.xlsx")

############################################################################
############################################################################

## Option 2: Divide students into smaller slots based on the city the stop is in

    ## 2 a) Reverse geocode and find the city the stop is in

gmaps = googlemaps.Client(key=Google_APIKEY)

stop_city = ['a']*len(students_df)

for i in range(len(stop_city)):
    geocoded_results = gmaps.reverse_geocode((students_df.iloc[i]['Lat'],students_df.iloc[i]['Lon']))
    geocoded_results = geocoded_results[0]['address_components']
    stop_city[i]     = geocoded_results[2]['long_name']

students_df['City'] = stop_city
students_df.to_excel("RI Mornings Cities Added.xlsx",index=False)

