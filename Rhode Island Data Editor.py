# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 15:48:56 2021

@author: d
"""

from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
import numpy as np
import pandas as pd
import datetime          as dt
import csv
import sys
import configparser
import requests
import json
import getopt
import time
import datetime
import folium
import math


df1 = pd.read_excel("D:/RI/PCCT_Mornings_Stops.xlsx")
df2 = pd.read_excel("D:/RI/Corner Stops V2.xlsx")
df3 = pd.read_excel("D:/RI/Mornings-Stops.xlsx")
df2 = df2[df2['Program Description'] != 'OOD Diverse Learners']
df3 = df3[df3['Program Description'] != 'OOD Diverse Learners']

student_num = []
stop_lat    = []
stop_lon    = []
school_name = []


for i in range(len(df1)):
    student_num.append(df1.iloc[i]['Student ID'])
    school_name.append(df1.iloc[i]['School Name'])
    stop_lat.append(df1.iloc[i]['new_stop_lat'])
    stop_lon.append(df1.iloc[i]['new_stop_lon'])
    
    
for i in range(len(df2)):
    student_num.append(df2.iloc[i]['Student ID'])
    school_name.append(df2.iloc[i]['School Name'])
    if (str(pd.isna(df2.iloc[i]['corner_lat'])) == "False"):
        stop_lat.append(df2.iloc[i]['corner_lat'])
        stop_lon.append(df2.iloc[i]['corner_lon'])
    else:
        stop_lat.append(df2.iloc[i]['am_stop_lat'])
        stop_lon.append(df2.iloc[i]['am_stop_lon'])

df3 = df3[~df3['Student ID'].isin(student_num)]
df3 = df3.reset_index()



for i in range(len(df3)):
    student_num.append(df3.iloc[i]['Student ID'])
    school_name.append(df3.iloc[i]['School Name'])
    stop_lat.append(df3.iloc[i]['am_stop_lat'])
    stop_lon.append(df3.iloc[i]['am_stop_lon'])



new_df = pd.DataFrame()

new_df['Student ID'] = student_num
new_df['Lat']        = stop_lat
new_df['Lon']        = stop_lon
new_df['School Name'] = school_name

new_df.to_excel("D:/PDPTW/RI Mornings.xlsx",index=False)


###############################################################################
###############################################################################


df = pd.read_excel("D:/PDPTW/RI Mornings.xlsx")
sdf = pd.read_excel("D:/RI/MorningsSLOT1.xlsx")

schools_ = list(sdf['SCHOOL NAME'])
df = df[df['School Name'].isin(schools_)]
df = df.reset_index()

## Assign time windows - 80 mins before school start


