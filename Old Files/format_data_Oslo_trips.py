#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 10:02:09 2019

@author: simonwilson
"""

"""
Import data and format for use by cvrptw.py
Created on Tue Nov  6 13:13:30 2018
@author: simonwilson
"""

from csv       import reader
from itertools import chain
from six.moves import xrange

import datetime          as dt
import matplotlib.pyplot as plt
import numpy             as np
import pandas            as pd

# #############################################################################
# USER DEFINED PARAMETERS 
# Where is the depot?
depot_location = (59.95,  10.77)
# How much deviation from requested pick up time are we allowed?
time_deviation_minutes = 5
# What's the capacity of a shuttle?  If we have location with more
# passengers than this then we'll split it up so that no booking has more
# than this number.
shuttle_capacity = 14
# #############################################################################

# #############################################################################
# LOAD DATA AND EXTRACT OUT RELEVANT COLUMNS
with open("D:/PDPTW/Old Files/07.03.2018-trips.csv",'r') as f:
   data = list(reader(f))      
f.close()
# Extract out the locations and scale them to be in metres
metres_per_degree_latitude  = 111395.0
metres_per_degree_longitude =  57475.2
# Extract out the pickup locations, dropoff locations and requested pickup times
locations_pickup_lat  = [x[12] for x in data]
locations_pickup_lon  = [x[13] for x in data]
locations_dropoff_lat = [x[14] for x in data]
locations_dropoff_lon = [x[15] for x in data]
times_pickup_requested = [dt.datetime.strptime(x[2],'%H:%M') for x in data]
# #############################################################################

# #############################################################################
# Aggregate bookings with the same values of pickup location, dropoff location
# and pickup time into one booking.  This reduces the number of bookings that the
# routing algorithm has to handle.
# First construct a frequency table of all unique combinations of these 5 
# variables. The counts in the table are the number of passengers to pick up or
# drop off at each unique combination.
data_cont_table = pd.crosstab([np.array(times_pickup_requested),
                               np.array(locations_pickup_lat),
                               np.array(locations_pickup_lon),
                               np.array(locations_dropoff_lat),
                               np.array(locations_dropoff_lon)],[])
n_bookings = len(data_cont_table)    
# Now we have to extract out the unique combinations of pickup, dropoff and 
# times.  These are the row names of data_cont_table in text format. We have
# to turn them into floating numbers (locations) and times, again in the format
# required by cvrptw.py  
# Extract out the row names (list of arrays of text)
cont_table_row_names = data_cont_table.axes[0].tolist()
# #############################################################################

# #############################################################################
# LOCATIONS
# Extract out the lat-long of pickup and dropoff coordinates and put them in
# metres.
# Get the pickup and dropoff coordinates from that list and map from lat-lon
# to metres (metres per degree longitude is appropriate for Oslo latitude)
locations_pickup  = [(float(c[1]),float(c[2])) for c in cont_table_row_names]
locations_dropoff = [(float(c[3]),float(c[4])) for c in cont_table_row_names]
# 'Weave' together the pick up and drop off locations so they appear next to 
# each other in the list for each passenger
locations_degrees = list(chain.from_iterable(zip(locations_pickup, 
                                         locations_dropoff)))
# Add in the depot
locations_degrees.insert(0,depot_location)
# What are the most westerly and southerly points in the data?
most_west  = min([min([loc[1] for loc in locations_pickup]),
                  min([loc[1] for loc in locations_dropoff])])
most_south = min([min([loc[0] for loc in locations_pickup]),
                  min([loc[0] for loc in locations_dropoff])])
# Derive location coordinates in metres from most west and south coordinate
locations = [(int(round((metres_per_degree_latitude*(loc[0]-most_south)))),
                   int(round(metres_per_degree_longitude*(loc[1]-most_west)))) for loc in locations_degrees]
# #############################################################################

# #############################################################################    
#  DEMANDS
# Extract out the demands in the quadrant
demands_pickup = [d[0] for d in data_cont_table.values.tolist()]   
# demands_pickup_in_quadrant = [demands_pickup[i] for i in range(n_bookings) if chosen_quadrant_flag[i] ]
demands_dropoff =  [-d for d in demands_pickup]    

# Weave the pickup and dropoff demands together so that they are in the right
# format for cvrptw.py
demands = list(chain.from_iterable(zip(demands_pickup,demands_dropoff)))
# Insert the demand from the depot (first element of list)
demands.insert(0,0)
# #############################################################################    

# #############################################################################
# TIME WINDOWS
# Extract out the pick up times.  Add in time windows.
# Get the pickup times and convert to datetime type
times_pickup_requested = [dt.datetime.strptime(dt.datetime.strftime(c[0],'%Y-%m-%d %H:%M:%S'),
                                               '%Y-%m-%d %H:%M:%S') for c in cont_table_row_names]
# Create windows of time 
# We allow a window either side of the requested pickup time
# Drop off times - no requested drop off time is given so we just put in a 
# wide time window from the start of the corresponding pick up window to plus
# 3 hours
time_deviation = dt.timedelta(minutes=float(time_deviation_minutes))
time_pickup_windows  = [(t - time_deviation,
                         t + time_deviation) for t in times_pickup_requested]  
time_dropoff_windows = [(time_pickup_windows[i][0],
                         time_pickup_windows[i][0] + dt.timedelta(minutes=60.0))
                          for i in range(len(times_pickup_requested))]
earliest_time = min(min(time_pickup_windows))
latest_time = max(max(time_dropoff_windows))
# Weave together the pickup and dropoff times 
time_windows_datetime = list(chain.from_iterable(zip(time_pickup_windows, 
                                                     time_dropoff_windows)))
# Add in the depot.  We never use its time window but just set it to be
# between the earliest and latest times
time_windows_datetime.insert(0,(earliest_time,latest_time))  
# String version of the times only (not dates) for printing
times_pickup_requested_timeonly_string = [str(dt.datetime.strptime(dt.datetime.strftime(c[0],'%Y-%m-%d %H:%M'),
                                               '%Y-%m-%d %H:%M').time()) for c in cont_table_row_names]
times_dropoff_timeonly_string =[" "] * len(time_dropoff_windows)
times_string = list(chain.from_iterable(zip(times_pickup_requested_timeonly_string,
                                            times_dropoff_timeonly_string)))
times_string.insert(0," ")  # add in the depot
# Finally convert the times to integer number of minutes from the earliest
# time appearing in the data 
time_windows_datetimedelta = [(t[0]-earliest_time, t[1]-earliest_time) for t in time_windows_datetime]
time_windows = [(int(round(t[0].total_seconds()/60)), int(round(t[1].total_seconds()/60))) for t in time_windows_datetimedelta]
# #############################################################################

# #############################################################################
# SPLIT ANY BOOKINGS THAT ARE LARGER THAN THE SHUTTLE CAPACITY
# Are any of the demands greater than shuttle_capacity?
# If so then split it up into bookings that are no greater than shuttle_capacity
for i in range(len(demands)):
    if demands[i] > shuttle_capacity:    # If i-th demand greater than shuttle capacity
        n_splits = 1 + (demands[i]-1)//shuttle_capacity  # How many bookings to split into?
        split_demands = [shuttle_capacity for n in range(n_splits-1)]
        # Firstt split booking is different size if needed: 
        split_demands.insert(0,demands[i]-sum(split_demands))
        # split_demands now is a list of demands[i] split as equally as possible
        # so that they do not exceed shuttle_capacity
        # Replicate the extra pick up and drop off locations and time_windows over the split demans
        split_locations         = list(chain.from_iterable(zip([locations[i]           for n in range(n_splits-1)],
                                                                [locations[i+1]        for n in range(n_splits-1)])))
        split_locations_degrees = list(chain.from_iterable(zip([locations_degrees[i]   for n in range(n_splits-1)],
                                                               [locations_degrees[i+1] for n in range(n_splits-1)])))
        split_time_windows      = list(chain.from_iterable(zip([time_windows[i]        for n in range(n_splits-1)],
                                                               [time_windows[i+1]      for n in range(n_splits-1)])))
        split_times_string      = list(chain.from_iterable(zip([times_string[i]        for n in range(n_splits-1)],
                                                               [times_string[i+1]      for n in range(n_splits-1)])))
        # Weave in the drop-off demands
        split_demands = list(chain.from_iterable(zip(split_demands,[-d for d in split_demands])))
        # Now add in the extra locations and time_windows
        locations[i:i] = split_locations
        locations_degrees[i:i] = split_locations_degrees
        time_windows[i:i] = split_time_windows
        times_string[i:i] = split_times_string
        # Remove the existing demands and replace with split_demands
        del demands[i:(i+2)]
        demands[i:i] = split_demands
# #############################################################################

# #############################################################################
# Plot the locations
locations_pickup_x  = [int(round(metres_per_degree_latitude*(loc[0]-most_south))) for loc in locations_pickup]
locations_pickup_y  = [int(round(metres_per_degree_longitude*(loc[1]-most_west))) for loc in locations_pickup]
locations_dropoff_x = [int(round(metres_per_degree_latitude*(loc[0]-most_south))) for loc in locations_dropoff]
locations_dropoff_y = [int(round(metres_per_degree_longitude*(loc[1]-most_west))) for loc in locations_dropoff]
plt.plot(locations_pickup_x, locations_pickup_y, 'r.',
         locations_dropoff_x,locations_dropoff_y,'g.') # ,
for i in range(len(locations_pickup)):
    plt.plot([locations_pickup_x[i],locations_dropoff_x[i]],
             [locations_pickup_y[i],locations_dropoff_y[i]],color="black")
         # int(round(metres_per_degree_longitude*(centre_EW-most_west))),int(round(metres_per_degree_latitude*(centre_NS-most_south))),'k+')
# #############################################################################

# #############################################################################

# #############################################################################
# Compute the distance matrix between the locations
# #############################################################################
def manhattan_distance(position_1, position_2):
  """Computes the Manhattan distance between two points"""
  return (
      abs(position_1[0] - position_2[0]) + abs(position_1[1] - position_2[1]))

distances = [[0] * len(locations) for i in range(len(locations))]
for from_node in range(len(locations)):
    for to_node in range(len(locations)):
       distances[from_node][to_node] = manhattan_distance(locations[from_node],locations[to_node])
                            



#      

#  
#def travel_time_simple(locations, from_node, to_node):
#    """Gets the travel times between two locations."""
#    speed=int(round(15*1000/60))
#    if from_node == to_node:
#      travel_time = 0
#    else:
#      travel_time = manhattan_distance(
#          locations[from_node],
#          locations[to_node]) / speed
#    return travel_time
#
#travel_times = []
#for from_node in range(n_bookings):
#    for to_node in range(from_node,n_bookings):
#        travel_times.append(travel_time_simple(locations,from_node,to_node))
#plt.hist(travel_times)       
