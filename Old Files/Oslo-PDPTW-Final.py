from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
import numpy as np
import pandas as pd
import format_data_Oslo_trips as fd
from six.moves import xrange
from itertools                 import chain
import datetime          as dt
from csv       import reader
import sys
sys.path.append("D:/PDPTW/Old Files/")


## Attempt 1: Adjust time windows
## Time windows can be thought of as arrival times at a particular node. The
## logic is that by reducing the time window for arrival, the alogrithm will
## automatically adjust the issue we have been facing
twin           = fd.time_windows[:]

for i in range(1,137,2):
    lower_bound = twin[i][0]
    upper_bound = twin[i][1]
    new_bound   = (lower_bound+1, upper_bound+7)
    twin[i+1]   = new_bound

dem  = fd.demands

vehicle_speed = 500
_distances = fd.distances
time_mat   = np.divide(_distances, vehicle_speed).tolist()

def create_data_model():
    """Stores the data for the problem."""
    data = {}
    data['distance_matrix'] = _distances
    data['time_matrix'] = time_mat
    
    
    data['vehicle_speed'] = 30*60/3.6

    data['time_windows'] = twin

    data['demands']      = fd.demands
    data['vehicle_capacities'] = [20] *20
    data['num_vehicles'] = 20
    data['depot'] = 0
    data['num_locations'] = len(data['time_matrix'])
    pickup_dropoff_indices = [[i,i+1] for i in range(1,data["num_locations"],2)]
    data["pickups_deliveries"] = pickup_dropoff_indices

    return data


def print_solution(data, manager, routing, assignment):
    """Prints assignment on console."""
    total_distance = 0
    time_dimension = routing.GetDimensionOrDie('Time')
    total_load     = 0
    total_time     = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        route_load     = 0
        while not routing.IsEnd(index):
            time_var = time_dimension.CumulVar(index)
            plan_output += '{0} Time({1},{2}) -> '.format(
                manager.IndexToNode(index), assignment.Min(time_var),
                assignment.Max(time_var))
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            plan_output += ' {0} Load( {1})-> '.format(node_index, route_load)
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            route_distance += (routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)) 
        time_var = time_dimension.CumulVar(index)
        plan_output += '{0} Time({1},{2})\n'.format(
            manager.IndexToNode(index), assignment.Min(time_var),
            assignment.Max(time_var))
        plan_output += 'Time of the route: {}min\n'.format(
            assignment.Min(time_var))
        plan_output += '{0} Load( {1})\n'.format(
                manager.IndexToNode(index), route_load)
        plan_output += 'Distance of the route: {}m\n'.format(route_distance)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
        total_time += assignment.Min(time_var)
    print('Total Distance of all routes: {}m'.format(total_distance))
    print('Total load of all routes:{}'.format(total_load))
    print('Total time of all routes: {} min'.format(total_time))

def print_save_solution(data,manager,routing, assignment):
    vehicle_id                  = [-1] * data["num_locations"]
    sequence_within_vehicle     = [0]  * data["num_locations"]
    # earliest_scheduled_time     = [-1] * (data.num_locations)
    # latest_scheduled_time       = [-1] * (data.num_locations)
    vehicle_cumulative_time     = [0]  * data["num_locations"]
    vehicle_cumulative_distance = [0]  * data["num_locations"]
    loadsfordf                  = [0] * (data['num_locations'])
    pick_drop = ["depot"] + ["pick","drop"] * (data["num_locations"] // 2)
    
    book_id = [0] + list(chain.from_iterable(zip([1+i for i in range(data["num_locations"] // 2) ],
                                                 [1+i for i in range(data["num_locations"] // 2) ])))
    passenger_time_in_shuttle = [0] * (data["num_locations"])
    # print('Objective: {}'.format(assignment.ObjectiveValue()))
    total_distance = 0
    total_load = 0
    total_time = 0
    capacity_dimension = routing.GetDimensionOrDie('Capacity')
    time_dimension = routing.GetDimensionOrDie('Time')
    for veh_id in xrange(data["num_vehicles"]):
       index = routing.Start(veh_id)
       distance = 0
       route_load = 0
       sequence_counter = 0 # Counts the sequence number of a location within the vehicle's route that it is assigned to
       while not routing.IsEnd(index):
          node_index = manager.IndexToNode(index)
          route_load += data['demands'][node_index]
          time_var  = time_dimension.CumulVar(index)
          # Put vehicle_id next to booking except if it's the depot
          if (index != routing.Start(veh_id)):
             vehicle_id[manager.IndexToNode(index)] = veh_id #############
          # earliest_scheduled_time[routing.IndexToNode(index)] = fd.earliest_time + dt.timedelta(minutes=float(assignment.Min(time_var)))
          # latest_scheduled_time[routing.IndexToNode(index)]   = fd.earliest_time + dt.timedelta(minutes=float(assignment.Max(time_var)))
          sequence_within_vehicle[manager.IndexToNode(index)] = sequence_counter
          vehicle_cumulative_time[manager.IndexToNode(index)] = (fd.earliest_time + dt.timedelta(minutes=float(assignment.Value(time_var)))).time()
          vehicle_cumulative_distance[manager.IndexToNode(index)] = distance
          loadsfordf[manager.IndexToNode(index)]                  = route_load
          previous_index   = index
          index            = assignment.Value(routing.NextVar(index))
          distance        += routing.GetArcCostForVehicle(previous_index, index, veh_id)*500    #Multiplying speed by time to get distance
          sequence_counter = sequence_counter+1
          # If this is a drop off, calculate the time that the passenger spent in the shuttle
          # Drop offs have an even index; their corresponding pick up has the previous index (minus 1)
          if index % 2 == 0:  # it's an even index so a drop off
              time_var_pickup = time_dimension.CumulVar(index-1)
              passenger_time_in_shuttle[manager.IndexToNode(index)] = float(assignment.Value(time_var)) - float(assignment.Value(time_var_pickup))
       load_var = capacity_dimension.CumulVar(index)
       time_var = time_dimension.CumulVar(index)
       total_distance += distance
       total_load += assignment.Value(load_var)
       total_time += assignment.Value(time_var)
    # earliest_scheduled_time = [t.time() for t in earliest_scheduled_time]
    # latest_scheduled_time   = [t.time() for t in latest_scheduled_time]
    data_for_df = [(book_id[i],fd.times_string[i],pick_drop[i],
                fd.locations_degrees[i][0],fd.locations_degrees[i][1],
                fd.demands[i],vehicle_id[i],sequence_within_vehicle[i],
                vehicle_cumulative_time[i],passenger_time_in_shuttle[i],
                vehicle_cumulative_distance[i],loadsfordf[i]) for i in range(len(fd.demands))]
    #            earliest_scheduled_time[i],latest_scheduled_time[i]
    data_df = pd.DataFrame(data=data_for_df,columns=["booking ID","requested time","pick or drop",
    #                                            "earliest_time","latest_time",
    #                                            "location_NS","location_EW",
                                                 "location (lat)","location (lon)",
                                                 "demand","vehicle ID","sequence",
                                                 "actual time","passenger journey time","cumulative distance (m)","vehicle load"])
    #                                            "earliest scheduled time","latest scheduled time"])
    data_df.sort_values(['vehicle ID','sequence'],ascending=[True,True],inplace=True)
    data_df["cumulative demand"]  = np.cumsum(data_df["demand"])
    pd.set_option('display.max_columns', 15)
    print(data_df)
    data_df.to_csv("Oslo-17mins.csv", index=False)


def main():
    """Entry point of the program."""
    # Instantiate the data problem.
    data = create_data_model()
    
    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(
        len(data['time_matrix']), data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)
    
    
    
    def time_callback(from_index, to_index):
        """Returns the travel time between the two nodes."""
        # Convert from routing variable Index to time matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['time_matrix'][from_node][to_node]
    
    transit_time_callback_index = routing.RegisterTransitCallback(time_callback)
    #routing.SetArcCostEvaluatorOfAllVehicles(transit_time_callback_index)
    
    
    def distance_callback(from_index, to_index):
        """Returns the manhattan distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node] 
    
    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    
    #distance = 'Distance'
    #routing.AddDimension(
        #transit_time_callback_index,
        #30,  # allow waiting time
        #500,  # maximum time per vehicle
        #False,  # Don't force start cumul to zero.
        #time)
    #time_dimension = routing.GetDimensionOrDie(time)

    
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]
    
    demand_callback_index = routing.RegisterUnaryTransitCallback(demand_callback)
    routing.AddDimensionWithVehicleCapacity(demand_callback_index,
                                            0, #nullcapacity slack
                                            data['vehicle_capacities'], #vehicle max capacities
                                            True, #start cumul to 0
                                            'Capacity')    
        
    time = 'Time'
    routing.AddDimension(
        transit_time_callback_index,
        30,  # allow waiting time
        1000,  # maximum time per vehicle
        False,  # Don't force start cumul to zero.
        time)
    time_dimension = routing.GetDimensionOrDie(time)
    
    
    # Add time window constraints for each location except depot.
    
    for location_idx, time_window in enumerate(data['time_windows']):
        if location_idx == 0:
            continue
        index = manager.NodeToIndex(location_idx)
        time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])
    # Add time window constraints for each vehicle start node.
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        time_dimension.CumulVar(index).SetRange(data['time_windows'][0][0],
                                                data['time_windows'][0][1])


    # Define Transportation Requests.
    for request in data['pickups_deliveries']:
        pickup_index = manager.NodeToIndex(request[0])
        delivery_index = manager.NodeToIndex(request[1])
        routing.AddPickupAndDelivery(pickup_index, delivery_index)
        routing.solver().Add(
            routing.VehicleVar(pickup_index) == routing.VehicleVar(
                delivery_index))
        routing.solver().Add(
            time_dimension.CumulVar(pickup_index) <=
            time_dimension.CumulVar(delivery_index))


    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.AUTOMATIC)
    
    search_parameters.time_limit.seconds = 30
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PARALLEL_CHEAPEST_INSERTION)
    
    
    
    search_parameters.log_search = True
    
    # Solve the problem.
    assignment = routing.SolveWithParameters(search_parameters)
    
    
    # Print solution on console.
    if assignment:
        print_solution(data, manager, routing, assignment)
        print_save_solution(data,manager,routing, assignment)

if __name__ == '__main__':
    main()
    
## Add passenger time to the .csv file
df = pd.read_csv("Oslo-17mins.csv")
max(df['booking ID']) #68 customers

passenger_time_spent = []
booking_ID           = []

for i in range(1,69):
    booking_ID.append(i)
    temp_df      = df.loc[df['booking ID'] == i]
    start_time   = temp_df.iloc[0][8]
    end_time     = temp_df.iloc[1][8]
    pickup_time  = dt.datetime.strptime(start_time, '%H:%M:%S')
    dropoff_time = dt.datetime.strptime(end_time, '%H:%M:%S')
    diff         = str(dropoff_time - pickup_time)
    passenger_time_spent.append(diff)



passengertime_dataframe = pd.DataFrame(passenger_time_spent)
passengertime_dataframe['booking ID'] = booking_ID
passengertime_dataframe.to_csv("TotalTimeSpentOslo.csv", index=False)
